### **Informação Técnica** ###

Thomson Modelo Technicolor TD5130 / Modem ADSL / Roteador WIFI LAN / 04 Portas Ethernet RJ45 

CPU1: Realtek RTL8672 / MIPS R3000 (big-endian)

FLA1: 4 MiB4,194,304 B , 32,768 Kib , 4,096 KiB , 32 Mib , 0.00391 GiB (Macronix 25L3206E)

RAM1: 32 MiB33,554,432 B , 262,144 Kib , 32,768 KiB , 256 Mib , 0.0313 GiB (Winbond W9825G6JH-6)

WI1 chip1: Realtek RTL8188RE bgn antenna connector: U.FL

ETH chip1: Realtek RTL8672

Switch: Realtek RTL8305N

LAN: 10/100 ports 4

Firmware Version: V2.05.C29GV

Outras informações em [1] [2] [3] [4] [5] [6] 

Data da pesquisa: 2014-07-30

### **Intro** ###

Encontrei esse modem-roteador ADSL da GVT dentro de uma caixa quando estava procurando um hub pra espetar na minha LAN. Achei ele bacana e então resolvi brincar um pouco com o firmware à procura de alguma forma para pegar shell no modem.

O firmware configurado para contas GVT é disponibilizado pelos proprietarios do modem no site deles[2]. A versão V2.05.C17GV[11] do site é um pouco antiga, mas foi a que eu usei para ter uma idéia de como é a origanização dos arquivos no fs. A versão do firmware do meu modem é a V2.05.C29GV (não achei na internet). Possivelmente a versão V2.05.C29GV foi atualizada através do protocolo TR-069 que é bastante usado pelas operadoras para manter o firmware dos aparelhos dos clientes atualizado.

### **Cenário** ###

As regras do cenário para o meu código eram:

1. O busybox->sh para ser chamado pela syscall do execve precisa que "/bin/busybox" esteja em argv0, args0 apontando pra "sh" e em args1 "NULL" pra o layout ficar execve("/bin/busybox",{&sh,NULL},NULL);.

2. Dup2(): dup2() é um wrapper para uma sequência de close()+fcntl() então preferi criar o código  dessa forma ao invés de usar a syscall do dup2 [tem aquele paper do tesoteam que falava de problemas com dup2 em alguns libc para sistemas embarcados].

O resultado da diversão: https://dl.dropboxusercontent.com/s/1ycxidgdylrgl1p/revsh.s 

### **Explorando** ###

Agora é só jogar o revshell compilado pra dentro do modem e executá-lo para abrir a shell em 192.168.1.2:13377.

Video
https://bytebucket.org/eremitah/td5130/raw/f5342a0f3438cc5d90a68420c853145207f2606d/td5130.ogv

--

Refências:

[1]  http://www.speedtouch.com.br/td5130.htm

[2]  https://wikidevi.com/wiki/Technicolor_TD5130

[3]  http://www.speedtouch.com.br/td5130/aqvtd5130/manuais/DS_Technicolor_TD5130.pdf

[4]  http://lista.mercadolivre.com.br/roteador-technicolor%2C-td5130%2C-gvt

[5]  http://logos.cs.uic.edu/366/notes/mips%20quick%20tutorial.htm

[6]  http://www.kneuro.net/cgi-bin/lxr/http/source/include/asm-mips/unistd.h?a=alpha#L995

[7]  http://www.boa.org/

[8]  http://landley.net/aboriginal/downloads/binaries/extras/dropbearmulti-mips

[9]  http://landley.net/aboriginal/downloads/binaries/extras/strace-mips

[10] https://dl.dropboxusercontent.com/s/1ycxidgdylrgl1p/revsh.s 

[11] http://www.speedtouch.com.br/td5130/aqvtd5130/firmware/TD5130A1_Auto-V2.05.C17GVD.zip